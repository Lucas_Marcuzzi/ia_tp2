﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AldeanoBehaviourTree : MonoBehaviour {

    public int capacity = 10;
    public int current;
    public int speed = 1;
    GameObject mine;
    GameObject house;
    public float gatheringDistance = 2;
    int nodenumber = 0;
    bool GotPath = false;
    List<GameObject> path;
    GameObject gridGen;
    float nodedistance = 0.5f;
    public bool AtHouse = false;
    public bool AtMine = false;

    // Use this for initialization
    void Start () {
        mine = GameObject.FindGameObjectWithTag("Mine");
        house = GameObject.FindGameObjectWithTag("House");
        gridGen = GameObject.FindGameObjectWithTag("GridGenerator");

    }
	
	// Update is called once per frame
	void Update () {

        GatherGold();
        DepositGold();
        Idle();
        GoToMine();
        GoToHouse();

    }

    void GatherGold()
    {
        if (current >= capacity)
            return;
        if (mine.GetComponent<Mine>().CheckEmpty())
            return;
        /*if (Vector3.Distance(transform.position, mine.transform.position) > gatheringDistance)
            return;*/
        if (!AtMine)
            return;

        Debug.Log("--------- Gathering Gold");
        current += mine.GetComponent<Mine>().GetGold(1);
    }
    void DepositGold()
    {
        if (current == 0)
            return;
        if (house.GetComponent<House>().CheckFull())
            return;
        /*if (Vector3.Distance(transform.position, house.transform.position) > gatheringDistance)
            return;*/
        if (!AtHouse)
            return;
        Debug.Log("--------- Depositing Gold");
        current -= house.GetComponent<House>().DepositGold(1);
    }
    void Idle()
    {
        if (!mine.GetComponent<Mine>().CheckEmpty())
            return;
        if (current > 0)
            return;
        Debug.Log("--------- Doing Nothing");
    }
    void GoToMine()
    {
        if (current >= capacity)
            return;
        if (mine.GetComponent<Mine>().CheckEmpty())
            return;
       /* if (Vector3.Distance(transform.position, mine.transform.position) < gatheringDistance)
            return;*/
        if (AtMine)
            return;

        Debug.Log("--------- Going to Gold");
        

        if (!GotPath)
        {
            gridGen.GetComponent<AStar>().FindPath(this.gameObject, mine);
            path = gridGen.GetComponent<AStar>().GetPath();
            GotPath = true;
            AtHouse = false;
            Debug.Log("StartingPoint: " + path[0].name);
            Debug.Log("EndPoint: " + path[path.Count-1].name);
        }

        Debug.Log("Going Mine NodeNumber: " + nodenumber + "/" + path.Count);

        if (Vector3.Distance(transform.position, path[nodenumber].transform.position) < nodedistance)
        {
            nodenumber++;
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        }
        else
        {
            transform.LookAt(path[nodenumber].transform);
            GetComponent<Rigidbody>().AddForce(transform.forward * speed);
        }
        if (nodenumber == path.Count-1)
        {
            //GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            nodenumber = 0;
            AtMine = true;
            //path.Clear();
            GotPath = false;
        }

    }

    void GoToHouse()
    {
        if (current < capacity && !mine.GetComponent<Mine>().CheckEmpty())
            return;
        /*if (Vector3.Distance(transform.position, house.transform.position) < gatheringDistance)
            return;*/
        if (AtHouse)
            return;

        Debug.Log("--------- Going Home");

        if (!GotPath)
        {
            gridGen.GetComponent<AStar>().FindPath(this.gameObject, house);
            path = gridGen.GetComponent<AStar>().GetPath();
            GotPath = true;
            AtMine = false;
            Debug.Log("StartingPoint: " + path[0].name);
            Debug.Log("EndPoint: " + path[path.Count-1].name);
        }

        Debug.Log("Going Home NodeNumber: " + nodenumber + "/" + path.Count);

        if (Vector3.Distance(transform.position, path[nodenumber].transform.position) < nodedistance)
        {
            nodenumber++;
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        }
        else
        {
            transform.LookAt(path[nodenumber].transform);
            GetComponent<Rigidbody>().AddForce(transform.forward * speed);
        }
        if (nodenumber == path.Count-1)
        {
            //GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            nodenumber = 0;
            //path.Clear();
            AtHouse = true;
            GotPath = false;
        }
    }

    void HeadTo(GameObject dest)
    {
        while(Vector3.Distance(transform.position, dest.transform.position) > gatheringDistance)
        {
            transform.LookAt(dest.transform);
            GetComponent<Rigidbody>().AddForce(transform.forward * speed);
        }
       GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
    }
}
