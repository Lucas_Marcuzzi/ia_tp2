﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyNodes : MonoBehaviour {

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Node")
            Destroy(other.gameObject);
    }
}
