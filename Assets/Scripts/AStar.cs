﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStar : MonoBehaviour {

    public GameObject StartGameObject;
    public GameObject EndGameObject;
    GridGenerator GridGen;
    List<GameObject> openNodes = new List<GameObject>();
    List<GameObject> closedNodes = new List<GameObject>();
    List<GameObject> path = new List<GameObject>();
    GameObject start;
    GameObject destination;
    public Mesh visualRepresentation;

	// Use this for initialization
	void Start () {
        GridGen = GameObject.FindGameObjectWithTag("GridGenerator").GetComponent<GridGenerator>();


    }
	
	// Update is called once per frame
	void Update () {
       /* FindPath(StartGameObject, EndGameObject);
        foreach (GameObject node in path)
        {
            node.GetComponent<Node>().MakeCube(visualRepresentation);
        }*/
    }
    public void FindPath (GameObject a_StartPos, GameObject a_TargetPos)
    {
        openNodes.Clear();
        closedNodes.Clear();
        path.Clear();

        //Debug.Log("Impl1");
        start = GridGen.GetClosestNode(a_StartPos);
        //Debug.Log("Impl2");
        destination = GridGen.GetClosestNode(a_TargetPos);
        GridGen.SetDestinationNode(destination);

        openNodes.Add(start);
        while(openNodes.Count>0)
        {
            GameObject CurrentNode = openNodes[0];
            for (int i =1;i<openNodes.Count; i++)
            {
                if(openNodes[i].GetComponent<Node>().Fcost < CurrentNode.GetComponent<Node>().Fcost || 
                    openNodes[i].GetComponent<Node>().Fcost == CurrentNode.GetComponent<Node>().Fcost && 
                    openNodes[i].GetComponent<Node>().hCost < CurrentNode.GetComponent<Node>().hCost)
                {
                    CurrentNode = openNodes[i];
                }
            }
            openNodes.Remove(CurrentNode);
            closedNodes.Add(CurrentNode);

            if(CurrentNode == destination)
            {
                GetFinalPath(start, destination);
            }

            foreach (GameObject NeighbourNode in GridGen.GetNeighbourNodes(CurrentNode))
            {
                if(closedNodes.Contains(NeighbourNode) || NeighbourNode==null)
                {
                    continue;
                }

                //Debug.Log("Hello its me");
                int MoveCost = CurrentNode.GetComponent<Node>().gCost + GetManhattanDistance(CurrentNode,NeighbourNode);

                    if (MoveCost < NeighbourNode.GetComponent<Node>().gCost || !openNodes.Contains(NeighbourNode))
                    {
                        NeighbourNode.GetComponent<Node>().gCost = MoveCost;
                        //Debug.Log("Hello its me Again");
                        NeighbourNode.GetComponent<Node>().hCost = GetManhattanDistance(NeighbourNode, destination);
                        NeighbourNode.GetComponent<Node>().Parent = CurrentNode;

                        if (!openNodes.Contains(NeighbourNode))
                        {
                            openNodes.Add(NeighbourNode);
                        }

                    }
                

            }

        }
    }

    private int GetManhattanDistance(GameObject currentNode, GameObject neighbourNode)
    {
        int ix = Mathf.Abs(currentNode.GetComponent<Node>().gridx - neighbourNode.GetComponent<Node>().gridx);
        int iy = Mathf.Abs(currentNode.GetComponent<Node>().gridy - neighbourNode.GetComponent<Node>().gridy);

        return ix + iy;
    }

    void GetFinalPath(GameObject a_StartingNode, GameObject a_DestinationNode)
    {
        List<GameObject> FinalPath = new List<GameObject>();
        GameObject CurrentNode = a_DestinationNode;

        while(CurrentNode!=a_StartingNode)
        {
            FinalPath.Add(CurrentNode);
            CurrentNode = CurrentNode.GetComponent<Node>().Parent;
        }
        FinalPath.Reverse();
        path = FinalPath;
    }
    public List<GameObject> GetPath()
    {
        return path;
    }
}
