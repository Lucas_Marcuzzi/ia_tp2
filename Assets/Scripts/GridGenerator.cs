﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridGenerator : MonoBehaviour {

    public int width;
    public int height;
    public int elevation=0;
    public int separation = 1;

    private GameObject mine;
    private GameObject house;
    private GameObject player;
    private GameObject nodes;
    private GameObject startNode;
    private GameObject destinationNode;

    GameObject[,] grid;

	// Use this for initialization
	void Start () {
        nodes = new GameObject("Nodes");
        GenerateGrid();
        mine = GameObject.FindGameObjectWithTag("Mine");
        house = GameObject.FindGameObjectWithTag("House");
        player = GameObject.FindGameObjectWithTag("Player");
        //startNode = GetClosestNode(player);
        //destinationNode = GetClosestNode(mine);
        //Debug.Log("StartNode: " + startNode.name);
        //Debug.Log("Destination: " + destinationNode.name);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void GenerateGrid()
    {
        int count = 0;
        grid = new GameObject[height, width];

        for (int i = 0; i<height;i++)
        {
            for(int e = 0; e<width;e++)
            {
                
                grid[i,e] = new GameObject(count.ToString());
                grid[i, e].GetComponent<Transform>().SetPositionAndRotation(new Vector3(i*separation,elevation, e * separation), grid[i, e].GetComponent<Transform>().rotation);
                grid[i, e].AddComponent<SphereCollider>();
                grid[i, e].GetComponent<SphereCollider>().isTrigger = true;
                grid[i, e].AddComponent<Rigidbody>();
                grid[i, e].GetComponent<Rigidbody>().isKinematic = true;
                grid[i, e].AddComponent<Node>();
                grid[i, e].GetComponent<Node>().SetNode(new Vector3(i * separation, elevation, e * separation),i,e);
                grid[i, e].tag = "Node";
                grid[i, e].transform.SetParent(nodes.transform);
                count++;
            }
        }
    }

    public GameObject GetClosestNode(GameObject obj)
    {
        GameObject closest = grid[0,0];
        //Debug.Log(closest.name);
        //Debug.Log(grid);
       // Debug.Log(obj.name);

        for (int i = 0; i < height; i++)
        {
            for (int e = 0; e < width; e++)
            {
                if(grid[i,e]!=null)
                if (Vector3.Distance(grid[i, e].transform.position, obj.transform.position) < (Vector3.Distance(closest.transform.position, obj.transform.position)))
                {
                    closest = grid[i, e];
                }
            }
        }

        return closest;
    }

    public List<GameObject> GetNeighbourNodes(GameObject a_Node)
    {
        List<GameObject> NeighbourNodes = new List<GameObject>();
        int xCheck;
        int yCheck;

        //Right Side
        xCheck = a_Node.GetComponent<Node>().gridx + 1;
        yCheck = a_Node.GetComponent<Node>().gridy;

        if(xCheck >= 0 && xCheck < width)
        {
            if(yCheck>=0 && yCheck < height)
            {
                NeighbourNodes.Add(grid[xCheck, yCheck]);
            }
        }

        //Left Side
        xCheck = a_Node.GetComponent<Node>().gridx - 1;
        yCheck = a_Node.GetComponent<Node>().gridy;

        if (xCheck >= 0 && xCheck < width)
        {
            if (yCheck >= 0 && yCheck < height)
            {
                NeighbourNodes.Add(grid[xCheck, yCheck]);
            }
        }

        //Top Side
        xCheck = a_Node.GetComponent<Node>().gridx;
        yCheck = a_Node.GetComponent<Node>().gridy + 1;

        if (xCheck >= 0 && xCheck < width)
        {
            if (yCheck >= 0 && yCheck < height)
            {
                NeighbourNodes.Add(grid[xCheck, yCheck]);
            }
        }

        //Down Side
        xCheck = a_Node.GetComponent<Node>().gridx;
        yCheck = a_Node.GetComponent<Node>().gridy - 1;

        if (xCheck >= 0 && xCheck < width)
        {
            if (yCheck >= 0 && yCheck < height)
            {
                NeighbourNodes.Add(grid[xCheck, yCheck]);
            }
        }

        return NeighbourNodes;

    }

    public GameObject GetDestination()
    {
        return destinationNode;
    }

    public GameObject GetStartNode()
    {
        //Debug.Log("Impl3");
        startNode = GetClosestNode(player);
        return startNode;
    }

    public float GetNodeDistance()
    {
        return separation;
    }
    
    public void SetDestinationNode(GameObject destination)
    {
        //Debug.Log("Impl4");
        destinationNode = GetClosestNode(destination);
    }

}
