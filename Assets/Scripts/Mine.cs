﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mine : MonoBehaviour {

	public int gold = 500;
    List<GameObject> Notify = new List<GameObject>();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public int GetGold(int ammount){
		int ammountwithdrawn;
		if (gold - ammount >= 0) {
			gold -= ammount;
			ammountwithdrawn = ammount;
            for(int i = 0; i< Notify.Count; i++)
            {
                if (Notify[i].GetComponent<UpdateSign>())
                    Notify[i].GetComponent<UpdateSign>().DoUpdateSign(gold);
            }
                
			return ammountwithdrawn;
		} else {
			ammountwithdrawn = gold;
			gold = 0;
            for (int i = 0; i < Notify.Count; i++)
            {
                if (Notify[i].GetComponent<UpdateSign>())
                    Notify[i].GetComponent<UpdateSign>().DoUpdateSign(gold);
            }
            return ammountwithdrawn;
		}
	}

	public bool CheckEmpty(){
		if (gold != 0)
			return false;
		else
			return true;
	}

    public void AddToNotify(GameObject meter)
    {
        Notify.Add(meter);
        if (meter.GetComponent<UpdateSign>())
           meter.GetComponent<UpdateSign>().DoUpdateSign(gold);
    }
}
