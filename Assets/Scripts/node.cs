﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour{

    public int gridx;
    public int gridy;

    public bool isWall;
    public Vector3 Position;
    public GameObject Parent;

    public int gCost;
    public int hCost;

    public int Fcost { get { return gCost + hCost; } }

    public void SetNode(Vector3 a_Pos, int a_gridX, int a_gridY)
    {
        Position = a_Pos;
        gridx = a_gridX;
        gridy = a_gridY;
    }

    public void MakeCube(Mesh _visual)
    {
        this.gameObject.AddComponent<MeshRenderer>();
        this.gameObject.AddComponent<MeshFilter>();
        this.gameObject.GetComponent<MeshFilter>().mesh = _visual;
    }
}
